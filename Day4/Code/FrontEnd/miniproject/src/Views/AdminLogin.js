import React, { useState } from "react";
import "./views.css";

export default function AdminLogin() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const [usernameErr, setUsernameErr] = useState(false);
  const [passwordErr, setPasswordErr] = useState(false);

  function usernameHandler(e) {
    let item = e.target.value;
    setEmail(item);

    if (item.length < 4) {
      setUsernameErr(true);
    } else {
      setUsernameErr(false);
    }
    setUsername(item);
  }

  function passwordHandler(e) {
    let item = e.target.value;
    if (item.length < 4) {
      setPasswordErr(true);
    } else {
      setPasswordErr(false);
    }
    setPassword("item");
  }

  function loginHandler(e) {
    if (password.length < 3 && username.length < 3) {
      alert("Invalid Data");
    } else if (username.length < 3) {
      alert("Invalid Email");
    } else if (password.length < 3) {
      alert("Invalid Password");
    }
    e.preventDefault();
  }

  /*------------------------------------------------------------------------*/
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");

  function emailValidation(e) {
    const regex = /[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,8}(.[a-z{2,8}])?/g;
    if (regex.test(email)) {
      setMessage("");
    } else if (!regex.test(email) && email !== "") {
      setMessage("Email is not valid");
    } else {
      setMessage("");
    }
  }

  return (
    <div className="container-fluid bgimage">
      <div
        className="row justify-content-center align-items-center"
        style={{ height: "100vh" }}
      >
        <div className="col-lg-4 col-sm-8 bg-secondary bg-opacity-50 p-3 rounded ">
          <form onSubmit={loginHandler}>
            <div>
              <h3 className="text-center text-white">ADMIN LOGIN</h3>
            </div>
            <div className="mb-1">
              <input
                className="form-control form-control-lg"
                type="text"
                placeholder="Enter Username"
                onChange={usernameHandler}
              />
              {usernameErr ? (
                <div class="validation">Enter email with minimum 3 characters</div>
              ) : null}
              <div class="validation">{message}</div>
            </div>
            <div className="mb-1">
              <input
                className="form-control form-control-lg"
                type="text"
                placeholder="Enter Password"
                onChange={passwordHandler}
              />
              {passwordErr ? (
                <div class="validation">Enter Password with minimum 3 characters</div>
              ) : null}
            </div>
            <div className="mb-1 p-3">
              <center>
                <button
                  type="submit"
                  class="btn btn-warning btn-lg w-75"
                  onClick={emailValidation}
                >
                  Login
                </button>
              </center>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

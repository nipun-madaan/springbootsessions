import { useState } from "react";
import {
  Button,
  Carousel,
  Container,
  Modal,
  Nav,
  Navbar,
  NavDropdown,
} from "react-bootstrap";


export default function Home() {
  return (
    <>
      <div className="sticky-top">
        <Navbar bg="secondary" expand="lg">
          <Container>
            <Navbar.Brand id="textcolor" className="fw-bold" href="/">
              LIBRARY MANAGEMENT SYSTEM
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="ms-auto fw-bold align-items-center">
                <Nav.Link id="textcolor" classname="p-2" href="/">
                  HOME
                </Nav.Link>
                <Nav.Link id="textcolor" href="/registration">REGISTER</Nav.Link>
                <NavDropdown title="LOGIN" id="basic-nav-dropdown">
                  <NavDropdown.Item href="/Studentlogin">
                    STUDENT LOGIN
                  </NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item href="/AdminLogin">
                    ADMIN LOGIN
                  </NavDropdown.Item>
                </NavDropdown>
                <Nav.Link id="textcolor" href="">F&Q</Nav.Link>
                <Example />
                
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </div>

      <Carousel>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src="https://quotefancy.com/media/wallpaper/3840x2160/4696797-Henry-Ward-Beecher-Quote-A-library-is-not-a-luxury-but-one-of-the.jpg"
            alt=""
          />
          <Carousel.Caption>
            <h3>One day, all your hard work will pay off.</h3>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src="http://quotefancy.com/media/wallpaper/3840x2160/4899-Plato-Quote-A-house-that-has-a-library-in-it-has-a-soul.jpg"
            alt=""
          />

          <Carousel.Caption>
            <h3>
              Every morning you have two choices: continue to sleep with your
              dreams, or wake up and chase them.
            </h3>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src="https://quotefancy.com/media/wallpaper/3840x2160/6361629-Francis-Bacon-Quote-Knowledge-is-power.jpg"
            alt=""
          />

          <Carousel.Caption>
            <h3>
              Nobody can go back and start a new beginning, but anyone can start
              today and make a new ending.
            </h3>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </>
  );
}

function Example() {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <>
      <Button id="textcolor" variant="secondary" onClick={handleShow}>
        ABOUT US
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>ABOUT DLI</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Digital Library of India (DLI) is a virtual repository of learning
          resources which is not just a repository with search/browse facilities
          but provides a host of services for the learner community. It is
          sponsored and mentored by CDAC,Mumbai, through its Mission on
          Education through Information and Communication Technology.
          Filtered and federated searching is employed to facilitate focused
          searching so that learners can find the right resource with least
          effort and in minimum time. NDLI provides user group-specific services
          such as Examination Preparatory for School and College students and
          job aspirants. Services for Researchers and general learners are also
          provided. NDLI is designed to hold content of any language and
          provides interface support for 10 most widely used Indian languages.
          It is built to provide support for all academic levels including
          researchers and life-long learners, all disciplines, all popular forms
          of access devices and differently-abled learners. It is designed to
          enable people to learn and prepare from best practices from all over
          the world and to facilitate researchers to perform inter-linked
          exploration from multiple sources. It is developed, operated and
          maintained from CDAC,Mumbai.
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}


package com.example.demo.service;

import com.example.demo.entity.User;

public interface UserService {

	public User register(User user);

	public User login(User user);
}

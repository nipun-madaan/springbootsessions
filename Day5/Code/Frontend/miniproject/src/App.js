import { BrowserRouter, Route, Routes } from "react-router-dom";
import StudentLogin from "./Views/StudentLogin";
import AdminLogin from "./Views/AdminLogin";
import Registration from "./Views/Registration";
import Home from "./Views/Home";
import AdminHome from "./Views/AdminHome";
import StudentHome from "./Views/StudentHome";

export default function app(){
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />}/>
          <Route path="/registration" element={<Registration />} />
          <Route path="/Studentlogin" element={<StudentLogin />} />
          <Route path="/Studenthome" element={<StudentHome />} />
          <Route path="/AdminLogin" element={<AdminLogin />} />
          <Route path="/Adminhome" element={<AdminHome />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

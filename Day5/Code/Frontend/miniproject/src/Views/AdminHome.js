import {
  Container,
  Nav,
  Navbar,
  NavDropdown,
} from "react-bootstrap";

export default function AdminHome() {
  return (
    <>
      <div
        id="bgadminhome"
        className="container-fluid"
        style={{ height: "100vh" }}
      >
        <div className="sticky-top">
          <Navbar bg="secondary" expand="lg" className="bg-opacity-50">
            <Container>
              <Navbar.Brand id="textcolor" className="fw-bold" href="/">
                LIBRARY MANAGEMENT SYSTEM
              </Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ms-auto fw-bold align-items-center">
                  <Nav.Link id="textcolor" classname="p-2 my-3" href="" style={{marginRight: "5vh"}}>
                    ADMIN PROFILE
                  </Nav.Link>
                  <NavDropdown title="STUDENT" id="basic-nav-dropdown" classname="my-3"  style={{marginRight: "5vh"}}>
                    <NavDropdown.Item href="/AdminLogin">
                      ADD STUDENT
                    </NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="/AdminLogin">
                     UPDATE STUDENT
                    </NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="/AdminLogin">
                      REMOVE STUDENT
                    </NavDropdown.Item>
                  </NavDropdown>
                  <NavDropdown title="BOOKS" id="basic-nav-dropdown" classname="my-3"  style={{marginRight: "5vh"}}>
                    <NavDropdown.Item href="/AdminLogin">
                      ADD BOOKS
                    </NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="/AdminLogin">
                     UPDATE BOOKS
                    </NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="/AdminLogin">
                     REMOVE BOOKS
                    </NavDropdown.Item>
                  </NavDropdown>
                  <Nav.Link id="textcolor" classname="p-2" href="/"  style={{marginRight: "5vh"}}>
                    LOGOUT
                  </Nav.Link>
                </Nav>
              </Navbar.Collapse>
            </Container>
          </Navbar>
        </div>
      </div>
    </>
  );
}

/*
function Popup() {
  <Modal.Dialog>
    <Modal.Header closeButton>
      <Modal.Title>Modal title</Modal.Title>
    </Modal.Header>

    <Modal.Body>
      <p>Modal body text goes here.</p>
    </Modal.Body>

    <Modal.Footer>
      <Button variant="secondary">Close</Button>
      <Button variant="primary">Save changes</Button>
    </Modal.Footer>
  </Modal.Dialog>;
}*/

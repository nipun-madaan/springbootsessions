import axios from "axios";
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";

export default function Registration() {
  const [name, setname] = useState("");
  const [mailID, setMailID] = useState("");
  const [phoneno, setPhoneno] = useState("");
  const [password, setPassword] = useState("");
  const [city, setCity] = useState("");
  const [pincode, setPincode] = useState("");

  const [nameerr, setnameerr] = useState(false);
  const [mailIDerr, setMailIDerr] = useState(false);
  const [phonenoerr, setPhonenoerr] = useState(false);
  const [msgerror, setMessageError] = useState(false);
  // const [msg,setMessage] = useState("");
  const [passworderr, setPassworderr] = useState(false);
  const [cityerr, setCityerr] = useState("");
  const [pincodeerr, setPincodeerr] = useState("");
  const navigate = useNavigate();
/*
  const registerStudent = () => {
    axios.post("http://localhost:8090/register").then(
      (response) => {
        alert(response.data);
      },
      (error) => {
        alert(error);
        }
    );
*/
 
  function nameHandler(e) {
    let item = e.target.value;

    if (item.length < 3) {
      setnameerr(true);
    } else {
      setnameerr(false);
    }
    setname(item);
  }

  function mailHandler(e) {
    let item = e.target.value;
    setEmail(item);

    if (item.length < 8) {
      setMailIDerr(true);
    } else {
      setMailIDerr(false);
    }
    setMailID(item);
  }

  function phonenoHandler(e) {
    let item = e.target.value;

    if (item.length !== 10) {
      setPhonenoerr(true);
    } else{
      setPhonenoerr(false);
    }
    setPhoneno(item);
  }

  function passwordHandler(e) {
    let item = e.target.value;

    if (item.length < 3) {
      setPassworderr(true);
    } else {
      setPassworderr(false);
    }
    setPassword(item);
  }

  function cityHandler(e) {
    let item = e.target.value;

    if (item.length < 3) {
      setCityerr(true);
    } else {
      setCityerr(false);
    }
    setCity(item);
  }
  
  function pincodeHandler(e) {
    let item = e.target.value;

    if (item.length < 3) {
      setPincodeerr(true);
    } else {
      setPincodeerr(false);
    }
    setPincode(item);
  }
  /*---------------------------------------------------------------------*/

  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");

  function emailValidation(e) {
    const regex = /[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,8}(.[a-z{2,8}])?/g;
    if (regex.test(email)) {
      setMessage("");
    } else if (!regex.test(email) && email !== "") {
      setMessage("Email is not valid");
    }
    setMessage("");
  }

  function registerUser(){
    let user = {
      name : name,
      email : mailID,
      phoneNo : phoneno,
      password : password,
      city : city,
      pincode : pincode
    }
    // console.log(user);
    axios.post("http://localhost:8080/register",user).then((response)=>{
      let msg = response.data;
      if(msg == "User registered successfully"){
        // <StudentLogin/>
        navigate("/Studentlogin",{message:msg});
      }else{
        setMessageError(true);
        setMessage(msg);
      }
    }).catch((error)=>{
      console.log(error);
    })
  }

  return (
    <div className="container-fluid bgimage">
      <div
        className="row justify-content-center align-items-center"
        style={{ height: "100vh" }}
      >
        <div className="col-lg-4 col-sm-8 bg-dark bg-opacity-50 p-3 rounded ">
          {/* <form> */}

            <div>
              <h3 className="text-center  text-white">REGISTRATION HERE</h3>
            </div>
            <div className="mb-1">
              <input
                className="form-control form-control-lg"
                type="text"
                placeholder="Enter Name"
                onChange={nameHandler}
              />
              {nameerr ? (
                <div className="validation">Name should be minimum of 3 characters</div>
              ) : null}
            </div>
            <div className="mb-1">
              <input
                className="form-control form-control-lg"
                type="text"
                placeholder="Enter EmailID"
                onChange={mailHandler}
              />
              {mailIDerr ? (
                <div className="validation">MailId should be minimum of 8 characters</div>
              ) : null}
              {/* <div className="validation">{message}</div> */}
            </div>
            <div className="mb-1">
              <input
                className="form-control form-control-lg"
                type="text"
                placeholder="Enter Phone No"
                onChange={phonenoHandler}
              />
              {phonenoerr ? (
                <div className="validation">Name should be of 10 digits</div>
              ) : null}
            </div>
            <div className="mb-1">
              <input
                className="form-control form-control-lg"
                type="password"
                placeholder="Set Password"
                 onChange={passwordHandler}
              />
               {passworderr ? (
                <div className="validation">MailId should be minimum of 3 characters</div>
              ) : null} 
            </div>
            <div className="mb-1">
              <input
                className="form-control form-control-lg"
                type="text"
                placeholder="Enter City"
                onChange={cityHandler}
              />
            </div>
            <div className="mb-1">
              <input
                className="form-control form-control-lg"
                type="text"
                placeholder="Enter Pincode"
                onChange={pincodeHandler}
              />
              </div>
            <div className="mb-1 p-3 justify-content-center">
              <center>
                <button onClick={registerUser} className="btn btn-warning btn-lg w-75">
                  REGISTER
                </button>
                {msgerror ? (
                <div className="validation">{message}</div>
              ) : null}
              </center>
            </div>
            <div className="text-center">
              <Link to="/StudentLogin" className="text-light">
                Existing User? LogIn
              </Link>
            </div>
          {/* </form> */}
        </div>
      </div>
    </div>
  );
}

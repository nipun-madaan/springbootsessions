import {
  Container,
  Nav,
  Navbar,
} from "react-bootstrap";

export default function StudentHome() {
  return (
    <>
      <div
        id="bgadminhome"
        className="container-fluid"
        style={{ height: "100vh" }}
      >
        <div className="sticky-top">
          <Navbar bg="secondary" expand="lg" className="bg-opacity-50">
            <Container>
              <Navbar.Brand
                id="textcolor"
                className="fw-bold"
                href="/Studenthome"
              >
                LIBRARY MANAGEMENT SYSTEM
              </Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ms-auto fw-bold align-items-center">
                  <Nav.Link
                    id="textcolor"
                    classname="p-2 my-3"
                    href=""
                    style={{ marginRight: "5vh" }}
                  >
                    STUDENT PROFILE
                  </Nav.Link>
                  <Nav.Link
                    id="textcolor"
                    classname="p-2"
                    href=""
                    style={{ marginRight: "5vh" }}
                  >
                    LIST OF BOOKS
                  </Nav.Link>
                  <Nav.Link
                    id="textcolor"
                    classname="p-2"
                    href=""
                    style={{ marginRight: "5vh" }}
                  >
                    RESERVE BOOK
                  </Nav.Link>
                  <Nav.Link
                    id="textcolor"
                    classname="p-2"
                    href="/"
                    style={{ marginRight: "5vh" }}
                  >
                    LOGOUT
                  </Nav.Link>
                </Nav>
              </Navbar.Collapse>
            </Container>
          </Navbar>
        </div>
      </div>
    </>
  );
}

/*
function Popup() {
  <Modal.Dialog>
    <Modal.Header closeButton>
      <Modal.Title>Modal title</Modal.Title>
    </Modal.Header>

    <Modal.Body>
      <p>Modal body text goes here.</p>
    </Modal.Body>

    <Modal.Footer>
      <Button variant="secondary">Close</Button>
      <Button variant="primary">Save changes</Button>
    </Modal.Footer>
  </Modal.Dialog>;
}*/

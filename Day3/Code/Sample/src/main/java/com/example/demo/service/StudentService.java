package com.example.demo.service;

import java.util.List;

import com.example.demo.DTO.StudentDTO;
import com.example.demo.entity.Student;

public interface StudentService {

	public Student addStudent(Student s);
	
	public List<Student> getStudents();
	
	public Student updateStudent(Integer studentId, StudentDTO student);
	
	public String deleteStudent(Integer studentId);
}

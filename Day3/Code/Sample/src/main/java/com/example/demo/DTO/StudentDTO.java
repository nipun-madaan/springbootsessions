package com.example.demo.DTO;

public class StudentDTO {
	private String name;
	
	private String email;
	
	private String mobileNo;
	
	private String course;
	
	private Integer marks;
	
	public StudentDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StudentDTO(String name, String email, String mobileNo, String course, Integer marks) {
		super();
		this.name = name;
		this.email = email;
		this.mobileNo = mobileNo;
		this.course = course;
		this.marks = marks;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public Integer getMarks() {
		return marks;
	}

	public void setMarks(Integer marks) {
		this.marks = marks;
	}
	
}

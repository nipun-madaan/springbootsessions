package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.DTO.StudentDTO;
import com.example.demo.entity.Student;
import com.example.demo.service.StudentService;

@RestController
@RequestMapping(value = "/student")
public class StudentController {

	@Autowired
	private StudentService studentService;
	
	@PostMapping("/addStudent")
	public String addStudent(@RequestBody Student s) {
		s = studentService.addStudent(s);
		if(s != null) {
			return "Student added successfully";
		}else {
			return "Student not added";
		}
	}
	
	@GetMapping("/getStudents")
	public List<Student> getStudents(){
		return studentService.getStudents();
	}
	
	@PutMapping("/updateStudent/{studentId}")
	public String updateStudent(@PathVariable Integer studentId,@RequestBody StudentDTO student) {
		Student s = studentService.updateStudent(studentId, student);
		if(s != null) {
			return "Student updated successfully";
		}else {
			return "Student not updated";
		}
	}
	
	@DeleteMapping("/deleteStudent/{studentId}")
	public String deleteStudent(@PathVariable Integer studentId) {
		return studentService.deleteStudent(studentId);
	}
}

package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.DTO.StudentDTO;
import com.example.demo.entity.Student;
import com.example.demo.repository.StudentRepository;

@Service
public class StudentServiceImpl implements StudentService{

	@Autowired
	private StudentRepository studentRepo;
	
	@Override
	public Student addStudent(Student s) {
		s = studentRepo.save(s);
		return s;
	}

	@Override
	public List<Student> getStudents() {
		return studentRepo.findAll();
		
	}

	@Override
	public Student updateStudent(Integer studentId, StudentDTO student) {
		// TODO Auto-generated method stub
		Optional<Student> studentOptional = studentRepo.findById(studentId);
		if(studentOptional.isPresent()) {
			Student s = studentOptional.get();
			if(student.getName() != null && !student.getName().isEmpty()) { //""
				s.setName(student.getName());
			}
			
			if(student.getEmail() != null && !student.getEmail().isEmpty()) {
				s.setEmail(student.getEmail());
			}
			
			if(student.getCourse() != null && !student.getCourse().isEmpty()) {
				s.setCourse(student.getCourse());
			}
			if(student.getMobileNo() != null && !student.getMobileNo().isEmpty()) {
				s.setMobileNo(student.getMobileNo());
			}
			if(student.getMarks() != null) {
				s.setMarks(student.getMarks());
			}
			
			s = studentRepo.save(s);
			return s;
		}
		return null;
	}

	@Override
	public String deleteStudent(Integer studentId) {
		Student s = studentRepo.getById(studentId);
		if(s != null) {	
			studentRepo.delete(s);
			return "Student deleted successfully";
		}else {
			return "Student not found";
		}
	}

	
}

package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.DTO.UserDTO;
import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;

@Service
//@Component
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository userRepo;
	
	@Override
	public User addUser(UserDTO userDTO) {
		User user =new User(userDTO.getName(),userDTO.getAddress(),userDTO.getEmail(),
				userDTO.getContactNo(),userDTO.getPassword());
		user = userRepo.save(user);  // insert data into DB
		return user;
	}

	@Override
	public User login(UserDTO userDTO) {
		// TODO Auto-generated method stub
		User user = userRepo.findByEmailAndPassword(userDTO.getEmail(), userDTO.getPassword());
		return user;
	}
}

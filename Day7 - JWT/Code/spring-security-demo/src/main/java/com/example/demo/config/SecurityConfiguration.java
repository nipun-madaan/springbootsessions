package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.SessionManagementConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.example.demo.service.UserService;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{

	@Autowired
	UserService userService;
	
	@Autowired
	JwtTokenHelper jwtTokenHelper;
	
	@Autowired
	RestAuthenticationEntryPoint authenticationEntryPoint;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// TODO Auto-generated method stub
//		super.configure(auth);
//		auth.inMemoryAuthentication().withUser("Nipun").password("nipun").authorities("USER","ADMIN");
		//in-memory
		//auth.inMemoryAuthentication().withUser("Nipun").password(passwordEncoder().encode("nipun")).authorities("USER","ADMIN");
		
		//database authentication
		auth.userDetailsService(userService).passwordEncoder(passwordEncoder());
		
	}
	
	@Bean
	PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception{
		return super.authenticationManagerBean();
	}
	@Override
	protected void configure(HttpSecurity http) throws Exception {
//		http.authorizeHttpRequests().anyRequest().permitAll();
//		http.authorizeHttpRequests().anyRequest().authenticated();
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and().exceptionHandling().authenticationEntryPoint(authenticationEntryPoint)
			.and().authorizeRequests((request)->request.antMatchers("/user/login").permitAll()
					.antMatchers("/user/**").hasAnyAuthority("USER","ADMIN")
					.antMatchers("/vendor/**").hasAuthority("VENDOR")
				.antMatchers(HttpMethod.OPTIONS,"/**").permitAll())
			.addFilterBefore(new JwtAuthenticationFillter(userService, jwtTokenHelper),UsernamePasswordAuthenticationFilter.class);
//		super.configure(http);
//		http.formLogin();
//		http.httpBasic();
		http.csrf().disable().cors().and().headers().frameOptions().disable();
	}

}

package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.example.demo.entity.Role;
import com.example.demo.entity.User;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;

@SpringBootApplication
public class SpringSecurityDemoApplication {

	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	UserRepository userRepo;
	
	@Autowired
	RoleRepository roleRepo;
	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityDemoApplication.class, args);
	}

	@PostConstruct
	protected void init() {
//		Role role = new Role();
//		role.setRoleName("VENDOR");
//		User user = new User();
//		user.setCity("Pune");
//		user.setEmail("akshay@gmail.com");
//		user.setEnabled(true);
//		user.setFullName("Akshay");
//		user.setPassword(passwordEncoder.encode("akshay123"));
//		user.setPhone("9812180092");
//		List<Role> roleList = new ArrayList<>();
//		roleList.add(role);
//		user.setRoleList(roleList);
////		role.setUser(user);
//		userRepo.save(user);
	}
	

}

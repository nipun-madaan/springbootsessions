package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.example.demo.entity.Role;
import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;

@SpringBootApplication
public class SpringSecurityDemoApplication {

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	UserRepository userRepo;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityDemoApplication.class, args);
	}

	@PostConstruct
	protected void init() {
		User user = new User();
		user.setName("Nipun Madaan");
		user.setAddress("Pune");
		user.setContactNo("9999999999");
		user.setEmail("nipun@gmail.com");
		user.setEnabled(true);
		user.setPassword(passwordEncoder.encode("nipun"));
		List<Role> roleList = new ArrayList<Role>();
		Role role = new Role();
		role.setRoleName("ADMIN");
		role.setDescription("admin");
		roleList.add(role);
		Role role1 = new Role();
		role1.setRoleName("USER");
		role1.setDescription("user");
		roleList.add(role1);
		user.setRoles(roleList);
		userRepo.save(user);
		
	}
}

package com.example.demo.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.DTO.UserDTO;

@RestController
@RequestMapping(value = "/user")
public class UserController {

	@RequestMapping(value="/",method=RequestMethod.GET)
	public String getIndex() {
		return "CDAC";
	}
	
	@PostMapping("/post")
	public String postMapping() {
		return "Post Mapping";
	}
	
	@PutMapping("/put")
	public String putMapping() {
		return "Put Mapping";
	}
	
	@DeleteMapping("/delete")
	public String deleteMapping() {
		return "delete Mapping";
	}
	
	@PostMapping("/addUser")
	public UserDTO addUser(@RequestBody UserDTO user) {
		return user;
	}
	
	@PostMapping("/add")
	public UserDTO addUser(@RequestParam Integer userId, @RequestParam String name,
			@RequestParam String address,@RequestParam String email, @RequestParam String contact ) {
		UserDTO user = new UserDTO(userId, name, address, contact, email);
		return user;
	}
	
	@GetMapping("/getUser/{userId}/{name}/{address}/{email}/{contact}")
	public UserDTO getUser(@PathVariable Integer userId, @PathVariable String name,
			@PathVariable String address,@PathVariable String email, @PathVariable String contact ) {
		UserDTO user = new UserDTO(userId, name, address, contact, email);
		return user;
	}
}
